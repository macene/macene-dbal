<?php

namespace DBAL;

use Exception;

class DBAL extends DB
{
	private $query = '';

	public function select($fields = array())
	{
		$query = 'SELECT ';

		if(empty($fields)){
			$query .= '*';
		}
		else{
			foreach($fields as $k => $v){
				$query .= $k . ' AS ' . $v . ', ';
			}
			$query = substr( $query, 0, -2);
		}

		$this->query .= $query;

		return $this;
	}

	public function from($table, $alias = null)
	{
		$this->query .= ' FROM ' . $table;
		if($alias != null){
			$this->query .= ' AS ' . $alias;
		}

		return $this;
	}

	public function where($key, $value, $match = '=')
	{
		$this->query .= ' WHERE ' . $key . ' ' . $match . ' "' . $this->escape($value) . '"';

		return $this;
	}

	public function andWhere($key, $value, $match = '=')
	{
		$this->query .= ' AND ' . $key . ' ' . $match . ' "' . $this->escape($value) . '"';

		return $this;
	}

	public function orWhere($key, $value, $match = '=')
	{
		$this->query .= ' OR ' . $key . ' ' . $match . ' "' . $this->escape($value) . '"';

		return $this;
	}

	public function whereArray($where, $method = 'AND')
	{
		if(!empty($where)){

			//var_dump($where);

			$this->query .= ' WHERE ';

			foreach($where as $k => $v){
				$this->query .= $v . ' ' . $method . ' ';
			}
			$this->query = substr( $this->query, 0, -4);
		}

		return $this;
	}

	public function sql($sql)
	{
		$this->query .= ' ' . $sql . ' ';

		return $this;
	}

	public function orderBy($row)
	{
		$this->query .= ' ORDER BY ';

		if(is_array($row)){
			foreach($row as $v){
				$this->query .= $this->escape($v) . ', ';
			}
			$this->query = substr( $this->query, 0, -2);
		}
		else{
			$this->query .= $row;
		}

		return $this;
	}

	public function limit($startPage, $numberPerPages)
	{
		$startPage = intval($startPage);
		$numberPerPages = intval($numberPerPages);
		$start = $startPage * $numberPerPages;
		$this->query .= ' LIMIT ' . $start . ',' . $numberPerPages;

		return $this;
	}

	public function join($table, $alias, $condition, $type = '')
	{
		if(in_array($type,array('left','right'))){
			$this->query .= ' ' . $type;
		}
		$this->query .= ' JOIN ' . $table . ' AS ' . $alias . ' ON ' . $condition;

		return $this;
	}

	public function getOne()
	{
		$data = $this->fetchSingle($this->query);
		if(!empty($data)){
			return $data;
		}
		else{
			return false;
		}
	}

	public function getAll()
	{
		$data = $this->fetchAll($this->query);
		if($data){
			return $data;
		}
		else{
			return false;
		}
	}

	public function insert($table, $data)
	{

		$fields = array();
		$contents = array();

		foreach($data as $key => $value){
			$fields[] = $key;
			$contents[] = "'".$this->escape($value)."'";
		}

		$this->query  = 'INSERT INTO ' . $table . ' ( '.implode(", ", $fields).' )
					VALUES ( '.implode(", ", $contents).' )';

		return $this;

	}

	public function update($table, $data)
	{

		$sqlSet = '';

		foreach($data as $key => $value){
			$sqlSet .= $key." = '".$this->escape($value)."', ";
		}

		$sqlSet = substr( $sqlSet, 0, -2);
		$this->query  = 'UPDATE ' . $table . ' SET '.$sqlSet.' ';

		return $this;

	}

	public function delete($table)
	{
		$this->query = 'DELETE FROM ' . $table . ' ';

		return $this;
	}

	public function persist()
	{
		if(!$this->execute($this->query)){
			return $this->error;
		}

		return true;
	}

	public function toString()
	{
		return $this->query;
	}

}
