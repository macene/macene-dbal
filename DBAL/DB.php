<?php
namespace DBAL;

use Mysqli;

class DB
{
    public $mysqli;
	public $numRows;
    public $debug;
    public $errno;
    public $error;
    
    public function __construct($host, $dbname, $user, $password)
    {
        $this->debug = 0;
	    $this->connect($host, $dbname, $user, $password);
    }

    
    private function __clone(){}

    
    public function connect($host, $dbname, $user, $password)
    {
	    $this->mysqli = @new mysqli( $host, $user, $password, $dbname );
        if (mysqli_connect_errno()){
            echo 'CONNECTION ERROR!<br />'.mysqli_connect_errno().": ".mysqli_connect_error();
        }
        $this->mysqli->set_charset("utf8");
    }
    
    
    public function escape($data)
    {
        //in caso ci siano le magic quotes abilitate(non è altro che un addslashes automatico) tolgo le slash prima 
        //di usare real_escape_string per non avere slash doppie
        $escaped = '';
        $data = (get_magic_quotes_gpc()) ? stripslashes($data) : $data;
        if( is_string($data) ){
            $escaped = $this->mysqli->real_escape_string($data);
        }
        return $escaped;
    }
    
    
    public function escapeArray($data, $exceptions = array())
    {
        if(is_array($data)){
            $escaped = array();
            foreach($data as $key => $value){
                //echo "$key - $value<br />";
                //in caso ci siano le magic quotes abilitate tolgo le slash prima 
                //di usare real_escape_string per non avere slash doppie
                $value = (get_magic_quotes_gpc()) ? stripslashes($value) : $value;
                if(!in_array($key, $exceptions) AND is_string($value) ){
                    $escaped[$key] = $this->mysqli->real_escape_string($value);
                }else{
                    $escaped[$key] = $value;
                }
            }
        }else{
            echo "No array in argument list";
        }
        return $escaped;
    }
    
    
    public function execute($query)
    {
        if($this->mysqli->query($query)){
            $this->showQuery($query, __FUNCTION__, 'ok');
            return true;
        }else{
            $this->showQuery($query, __FUNCTION__, 'error');
            $this->errno = $this->mysqli->errno;
            $this->error = $this->mysqli->error;
            return false;
        }
    }

    
    public function fetchColumn($query)
    {
        if($result = $this->mysqli->query($query)){
            $this->numRows = $result->num_rows;
            if($result->num_rows){
                if($row = $result->fetch_array()){
                    $result->free();
                }
                $this->showQuery($query, __FUNCTION__, 'ok');
                return $row[0];
            }else{
                $this->showQuery($query, __FUNCTION__, 'warning');
                return false; // $row[0] potrebbe avere valore 0 quindi il confronto in caso di false va fatto con ===
            }
        }else{
            $this->showQuery($query, __FUNCTION__, 'error');
            $this->errno = $this->mysqli->errno;
            $this->error = $this->mysqli->error;
            return -1;//lasciare -1?
        }
    }

    
    public function fetchAll($query, $fetch_object = false, $fetch_assoc = true)
    {
        if($result = $this->mysqli->query($query)){
            $this->numRows = $result->num_rows;
            if($result->num_rows){
                $data = array();
                if($fetch_object){
                    while($row = $result->fetch_object()){
                        $data[] = $row;
                    }
                }else{
                    if($fetch_assoc){
                        while($row = $result->fetch_assoc()){
                            $data[] = $row;
                        }
                    }else{
                        while($row = $result->fetch_row()){
                            $data[] = $row;
                        }
                    }
                }
                $this->showQuery($query, __FUNCTION__, 'ok');
                $result->free();
                return $data;
            }else{
                $this->showQuery($query, __FUNCTION__, 'warning');
                return false;
            }
        }else{
            $this->showQuery($query, __FUNCTION__, 'error');
            $this->errno = $this->mysqli->errno;
            $this->error = $this->mysqli->error;
            return false;
        }
    }
    
    
    public function fetchAllPlain($query)
    {
        if($result = $this->mysqli->query($query)){
            $this->numRows = $result->num_rows;
            if($result->num_rows){
                $data = array();
                while($row = $result->fetch_row()){
                    $data[] = $row[0];
                }
                
                $this->showQuery($query, __FUNCTION__, 'ok');
                $result->free();
                return $data;
            }
            
        }else{
            $this->showQuery($query, __FUNCTION__, 'error');
            $this->errno = $this->mysqli->errno;
            $this->error = $this->mysqli->error;
            return false;
        }
    }

    
    public function fetchSingle($query, $fetch_object = false)
    {
        if($result = $this->mysqli->query($query)){
            $this->numRows = $result->num_rows;
            if($result->num_rows){
                if($fetch_object){
                    $row = $result->fetch_object();
                }else{
                    $row = $result->fetch_assoc();
                }
                $result->free();
                $this->showQuery($query, __FUNCTION__, 'ok');
                return $row;
            }else{
                $this->showQuery($query, __FUNCTION__, 'warning');
                return false;
            }
        }else{
            $this->showQuery($query, __FUNCTION__, 'error');
            $this->errno = $this->mysqli->errno;
            $this->error = $this->mysqli->error;
            return false;
        }
    }


    private function showQuery($query, $function, $type)
    {
        if($this->debug){
            $color = array( 'ok' => '#328417', 'warning' => '#FDD', 'error' => '#EA2E30'); 
            echo "<b style='color:".$color[$type]."'>$function:<br />".$query;
            
            if($type == 'error'){
                echo "<br/><span style='font-size:14px;font-weight:normal'>".$this->mysqli->errno.": ".$this->mysqli->error."</span><br/>";
            }
            
            echo "</b><br /><br />";
        }
    }
    
    
    public function paginate($totalRows, $offset, $tablePage, $lang=1, $site=1 )
    {
        $pages = ceil($totalRows/$offset);
        //echo $pages."/".$totalRows."/".$offset;
        
        if($pages > 1){

            echo '<ul class="pager">';
            /*
            $prev =  ( $tablePage != 1 ) ? $tablePage - 1 : NULL;
            if(!is_null($prev)){
                echo "<li><a href='$pageName?tablePage=$prev'>‹</a></li>";
            }
            */
            $i = 1;
            while ($i <= $pages) {

                if ( $i == $tablePage ) { // sono nella pagina corrente
                    echo "<li class='active'><a href='#'>$i</a></li>";
                } else {
                    echo "<li><a href='?l=$lang&tablePage=$i'>$i</a></li>";
                }
                $i++;
            }
            /*
            $next =  ( $tablePage < $pages ) ? $tablePage + 1 : NULL;
            if(!is_null($next)){
                echo "<li><a href='$pageName?tablePage=$next'>›</a></li>";
            }
            */
            echo "</ul>";
        }
    }
    
    
    public function MLPaginate($totalRows, $offset, $tablePage, $itemId, $module)
    {
        $pages = ceil($totalRows/$offset);
        //echo $pages."/".$totalRows."/".$offset;
        
        if($pages > 1){

            echo '<ul class="pager">';
            /*
            $prev =  ( $tablePage != 1 ) ? $tablePage - 1 : NULL;
            if(!is_null($prev)){
                echo "<li><a href='$pageName?tablePage=$prev'>‹</a></li>";
            }
            */
            $i = 1;
            while ($i <= $pages) {

                if ( $i == $tablePage ) { // sono nella pagina corrente
                    echo "<li><a class='sel' href='#'>$i</a></li>";
                } else {
                    echo "<li><a href='?tablePage=$i&amp;module=$module&amp;itemId=$itemId'>$i</a></li>";
                }
                $i++;
            }
            /*
            $next =  ( $tablePage < $pages ) ? $tablePage + 1 : NULL;
            if(!is_null($next)){
                echo "<li><a href='$pageName?tablePage=$next'>›</a></li>";
            }
            */
            echo "</ul>";
        }
    }

    
    public function safeData($data, $length = 65000, $tag = false)
    {
		$data = $this->mysqli->real_escape_string(trim($data));
		if(!$tag){
			$data = strip_tags($data);
		}
		$data = substr($data, 0, $length);
		return $data;
	}

	
	public function cleanData($data, $html = true)
	{
		$data = trim($data);
		$data = stripslashes($data);
		if($html){
			$data = htmlentities($data, ENT_QUOTES, "UTF-8");
		}
		return $data;
	}

	
	public function cleanDataXml($data)
	{ //meglio usare CDATA!!!!
		$data = $this->cleandata($data, false);
		$data =  str_replace("&#039;","'",$data);
		$data =  str_replace("&lsquo;","'",$data);
		$data =  str_replace("&rsquo;","'",$data);
		$data =  str_replace("&ldquo;",'"',$data);
		$data =  str_replace("&rdquo;",'"',$data);
		$data =  str_replace("&hellip;",'...',$data);
		$data =  str_replace("&ndash;",'-',$data);
		$data =  str_replace("\n",'',$data);
		//$data = nl2br($data);
		return preg_replace('/[^!-%\x27-;=?-~ ]/e', '"&#".ord("$0").chr(59)', $data);
	}

	
    public function insertId()
    {
        return $this->mysqli->insert_id;
    }
    
    
    public function errno()
    {
        return $this->mysqli->errno;
    }
    
    
    public function error()
    {
        return $this->mysqli->error;
    }
    
    
    public function affectedRows()
    {
        return $this->mysqli->affected_rows;
    }

    
    public function numRows()
    {
        return $this->numRows;
    }

    
    public function close()
    {
        if($this->mysqli){
            $this->mysqli->close();
            $this->mysqli = false;
        }
    }
    
    function __destruct()
    {
        $this->close();
    }

}