# macene DBAL

Lightweight MySQL Database Abstraction Layer.
Create query as you are writing SQL, just easier.

## Install

Install composer in your project:

	curl -s https://getcomposer.org/installer | php

Create a composer.json file in your project root or edit the existing composer.json:

	{
	    "repositories": [
	        {
	            "type": "git",
	            "url": "https://bitbucket.org/macene/macene-dbal"
	        }
	    ],
	    "require": {
	        "macene/macene-dbal": "dev-master"
	    }
	}

Install composer in your project:

	php composer.phar install // if this is the first install
	php composer.phar update // if you already used composer for other libraies

Add this line to your application’s index.php file:

	<?php
	$db = new \DBAL\DBAL($host,$dbname,$dbuser,$dbpassword);


## Usage Examples

### Query Example

	<?php
	$db ->select()
		->from('table_name')
		->where('id', 1)
		->getAll();

### Select

	<?php
	$select = array( 'name' => 'full_name' );
	$db ->select(); => "SELECT * "
	$db ->select($select); => "SELECT name as full_name "

### From

	<?php
	$db ->from('table_name') => "FROM table_name"

### Where

	<?php
	$db ->where('id', '1') => "WHERE id = '1'"
	$db ->where('id', '1', '>') => "WHERE id > '1'"
	$db ->andWhere('id', '2') => "AND id = '2'"
	$db ->orWhere('id', '3') => "OR id = '3'"
	$db ->whereArray( array("id = 1","name LIKE '%tom%'"), 'OR') => "WHERE id = '1' OR name LIKE '%tom%'"

### Order by

	<?php
	$db ->orderBy('id ASC') => "ORDER BY id ASC"
	$db ->orderBy( array( 'id ASC','name ASC' ) ) => "ORDER BY id ASC, name ASC"

### Limit

	<?php
	$db ->limit(start page, number per pages) => "LIMIT 6,3 "
	$db ->limit(2,3) => "LIMIT 6,3 "

### Fetching

	<?php
	$db ->getOne() returns 1 row
	$db ->getAll() returns all the rows

### Insert

	<?php
	$data = array('name'=>'Tom','surname'=>'Jerry');
	$db ->insert('table', $data)->persist() => "INSERT INTO table (name,surname) VALUES ('Tom','Jerry')"

### Update

	<?php
	$data = array('name'=>'Tom','surname'=>'Jerry');
    $db ->insert('table', $data) => "UPDATE table SET name='Tom', surname='Jerry' "

### Delete

	<?php
	$db ->delete('table') => "DELETE FROM table "
